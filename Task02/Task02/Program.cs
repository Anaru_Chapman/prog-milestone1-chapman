﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task02
{
    class Program
    {
        static void Main(string[] args)
        {
            // Get user input for the month and day they are born on and display it in a sentence

            string month = " ";
            int day = 0;

            Console.WriteLine("Hello, Please enter the name of the month in which you were born (January ..)");
            month = Console.ReadLine();

            Console.WriteLine("Please enter the date on which you were born as a number eg 1 , 2, etc ");
            day = int.Parse(Console.ReadLine());

            Console.WriteLine("Thank you");
            Console.WriteLine($"You were born on the {day} of {month}");
        }
    }
}
