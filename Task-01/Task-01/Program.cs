﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_01
{
    class Program
    {
        static void Main(string[] args)
        {
            string userName = "";
            int userAge = 0;

            Console.WriteLine("Hello, what is your name?");
            userName = Console.ReadLine();


            Console.WriteLine("What is your age?");
            userAge = int.Parse(Console.ReadLine());

            Console.WriteLine("Thank you ");
            Console.WriteLine("Your name is " + userName + " and your age is " + userAge.ToString());

        }
    }
}
