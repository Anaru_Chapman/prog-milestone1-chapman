﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_09
{
    class Program
    {
        static void Main(string[] args)
        {
            for (int i = 2016; i < 2037; i++)
            { if(i % 4 == 0)
                {
                    Console.WriteLine($"{i} is a leap year");
                }
            }
        }
    }
}
