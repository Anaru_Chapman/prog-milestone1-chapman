﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task03
{
    class Program
    {
        static void Main(string[] args)
        {
            //Using if statements and / or switches convert KM to Miles and MIles to KM

            int choice = 0;
            double totalToConvert = 0.0;
            double convertedTotal = 0.0;
            double milesTokm = 1.60934;
            double kmTomiles = 0.621;
           
            Console.WriteLine("");
            Console.WriteLine("Welcome to the converter");
            Console.WriteLine("Would like to convert:");
            Console.WriteLine("1. Miles to Km");
            Console.WriteLine("2. Km to Miles");
            Console.WriteLine("");
            Console.WriteLine("Enter 1 or 2");
            choice = int.Parse(Console.ReadLine());


            if (choice == 1)
            {   Console.WriteLine("Enter number of Miles to convert: ");
                totalToConvert = double.Parse(Console.ReadLine());
                convertedTotal = totalToConvert * milesTokm;
                Console.WriteLine($"Thank you:{totalToConvert} miles is equal to  {convertedTotal} km ");
            }

            else if (choice == 2)
            {   Console.WriteLine("Enter number of km to convert: ");
                totalToConvert = double.Parse(Console.ReadLine());
                convertedTotal = totalToConvert* kmTomiles;
                Console.WriteLine($"Thank you:{totalToConvert} km is equal to  {convertedTotal} miles ");
              
            }

            else
            { Console.WriteLine("Choice must be 1 or 2");

            }

        }
    }
}
