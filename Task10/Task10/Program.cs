﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task10
{
    class Program
    {
        static void Main(string[] args)
        {
            int counter = 0;

            for (int i = 2016; i < 2037; i++)
            {
                if (i % 4 == 0)
                {
                    Console.WriteLine($"{i} is a leap year");
                    counter++;
                }
            }
            Console.WriteLine($"There are {counter} leap years in the next 20 years");
        }
    }
}
