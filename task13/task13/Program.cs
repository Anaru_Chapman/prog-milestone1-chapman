﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task13
{
    class Program
    {
        static void Main(string[] args)
        {
            //Mini shop - the sum of  3 doubles by user input and add GST (15%) 
            //Output must show $ signs

            double num1 = 0;
            double num2 = 0;
            double num3 = 0;
            double total = 0;

            Console.WriteLine("Welcome to the mini shop");
            Console.WriteLine("Please enter first number:");
            num1 = double.Parse(Console.ReadLine());
            Console.WriteLine($"Thank you the first number is:{num1} ");
            Console.WriteLine("Please enter second number:");
            num2 = double.Parse(Console.ReadLine());
            Console.WriteLine($"Thank you the second number is:{num2} ");

            Console.WriteLine("Please enter third number:");

            num3 = double.Parse(Console.ReadLine());
            Console.WriteLine($"Thank you the third number is:{num3} ");
            Console.Clear();
            Console.WriteLine(num1);
            Console.WriteLine(num2);
            Console.WriteLine(num3);
            Console.WriteLine("");
            total = (num1 + num2 + num3) * 1.15;
            Console.WriteLine($" The total plus GST is ${total}");
        }
    }
}
