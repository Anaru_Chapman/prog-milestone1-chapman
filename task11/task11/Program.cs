﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task11
{
    class Program
    {
        static void Main(string[] args)
        {
            int y;
            Console.WriteLine(" *** Welcome to the Leap Year Checker ***");
            Console.WriteLine("Please enter the four digit year that you would like to check");

            y = Convert.ToInt32(Console.ReadLine());

            if ((y % 4 == 0 && y % 100 != 0) || (y % 400 == 0)) // centenary years not divisible by 400 are not leap years
            {
                Console.WriteLine("{0} is a Leap Year", y);
            }
            else
            {
                Console.WriteLine("{0} is not a Leap Year", y);
            }
        }
    }
}
